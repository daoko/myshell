/*
 * Written by Alex Case
 * Project 2: Threads
 */

import java.io.*;
import java.net.URL;
import java.util.*;

public class MyShell {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws InterruptedException
	{
		System.out.println("MyShell starting...");
		
		
		Scanner in = new Scanner(System.in);
		//StringTokenizer is used to split up commands
		StringTokenizer splitter;
		
		//Keep track of command history
		ArrayList<String> history = new ArrayList<String>();
		
		//Keep track of the current path, 
		//set by default to where this program is run
		URL location = MyShell.class.getProtectionDomain().getCodeSource().getLocation();
		File currentDirectory = new File(location.getPath());
		
		//Main Loop
			while(true)
			{
				
				System.out.print(currentDirectory.getAbsolutePath() +">");
				//Record next line with the scanner
				String commandLine = in.nextLine();
				//Split the input string into single commands
				splitter = new StringTokenizer(commandLine, "&");
				
				//Split string into separate arguments
				while(splitter.hasMoreTokens())
				{
				String next = splitter.nextToken().trim();
				//System.out.printf("x%sx%n", trimmed);
				
				//Check for exit command
				if( next.equals("exit")){
					System.out.println("Exit command received, MyShell closing");
					System.exit(0);
				}
				//Check for history command
				else if( next.equals("history")){
					System.out.println("Command History");
					for( String e: history)
						System.out.println(e);
					break;
				}
				//Check for repeat command
				else if(next.startsWith("!")){
					int num = Integer.parseInt( next.split("!")[1]);
					next = history.get(--num);
				}
				//Check for cd command
				else if( next.subSequence(0, 2).equals("cd")){

					StringTokenizer splitArgs = new StringTokenizer(next);
					splitArgs.nextToken(); //isolate the path argument
					String path = splitArgs.nextToken();
					
					//Parent Directory
					if (path.equals("..")){
						currentDirectory = currentDirectory.getParentFile();
						
					}
					//Relative directory
					else if(!path.contains("\\")){
						String newPath = currentDirectory.getAbsolutePath().concat("\\" + path);					
						currentDirectory = new File(newPath);
						
					}
					//Absolute Directory
					else {
						currentDirectory = new File(path);	
					}
				}
				//Add the command to history
				history.add(next);	
				//Create a new thread and run it
				Command thread = new Command(next, currentDirectory);
				Thread t = new Thread(thread);
				t.start();
				t.join();
				}		
			}
			
	}
	
}
