/*
 * Written by Alex Case
 * Project 2: Threads
 */

import java.io.*;

public class Command implements Runnable {

	String command;
	File currentWorkingDirectory;

    public Command(String c, File cwd){
    	command = c;
    	currentWorkingDirectory = cwd;
    }
   
    public void run(){

    	ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
	    builder.redirectErrorStream(true);
	    builder.directory(currentWorkingDirectory);
	    try{
		    Process p = builder.start();
		    BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    String line;
		    while (true) {
		        line = r.readLine();
		        if (line == null) { break; }
		        System.out.println(line);
		    }
	    } catch (Exception e){ e.printStackTrace(); }
	    
	    return;
    
    }
    
}